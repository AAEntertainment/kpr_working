﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldObject : MonoBehaviour
{
    virtual public float VerticalPosition
    {
        set { transform.localPosition = new Vector3(0, value, 0); }
        get { return transform.localPosition.y; }
    }

    virtual public Vector3 HorizontalRotationDirection
    {
        get { return transform.forward; }
    }

    virtual public float HorizontalAngularDistanceTo(WorldObject other)
    {
        if (other != null)
            return Mathf.Lerp(180, 0, (Vector3.Dot(HorizontalRotationDirection, other.HorizontalRotationDirection) + 1) * 0.5f);
        return -1;
    }

    virtual public float VerticalDistanceTo(WorldObject other)
    {
        if (other != null)
            return Mathf.Abs(other.VerticalPosition - VerticalPosition);
        return -1;
    }

    public void Move(float moveSpeed, float direction)
    {
        transform.RotateAround(Vector3.zero, new Vector3(0, 1, 0), (-Time.deltaTime * moveSpeed * direction));
    }

    virtual public void GetDebugInfo(ref string outString)
    {
    }

}
