﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventVector2 : UnityEvent<Vector2>
{
}

public class PlayerVehicleCore : VehicleCore
{
    virtual public void ApplyInputAlpha(Vector2 input) { }
    virtual public void PressTopButton() { }
    virtual public void ReleaseTopButton() { }
    virtual public void PressBottomButton() { }
    virtual public void ReleaseBottomButton() { }
    virtual public float CameraHeightAlpha { get { return 0.0f; } }
}
