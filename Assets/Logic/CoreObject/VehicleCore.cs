﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VehicleCore : MonoBehaviour
{
    DamageReceiver health;

    public void HealthSetup(UnityAction onDamageTaken, UnityAction onDamageDestroyed)
    {
        health = GetComponent<DamageReceiver>();
        if(health)
        {
            health.onDamageTaken.AddListener(onDamageTaken);
            health.onDamageDestroyed.AddListener(onDamageDestroyed);
        }
    }

    virtual public Vector3 Velocity
    {
        get { return new Vector3(); }
    }

    virtual public float MoveDirection
    {
        get { return 0; }
    }

    virtual public void GetDebugInfo(ref string outString)
    {
    }
}
