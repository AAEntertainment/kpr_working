﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeppCore : MonoBehaviour
{
    public GameObject engine;
    public GameObject shootAt;
    public ZeppBullet bulletType;

    public int shootAmount = 10;
    public float shootCooldown = 3;
    public float moveAmount = 6;
    public float moveSpeed = 0.5f;

    float offsetAmount;
    Vector3 startPosition;
    float currentCooldown = 0;
    int currentShootAmount = 0;

    void Start()
    {
        startPosition = gameObject.transform.position;
        offsetAmount = Random.Range(-Mathf.PI, Mathf.PI);
        currentCooldown = Random.Range(0, 10);
    }

    void Update()
    {
        if(engine != null)
            engine.transform.rotation = engine.transform.rotation * Quaternion.Euler(0, 0, Time.deltaTime * 1000);

        offsetAmount += Time.deltaTime * moveSpeed;
        if (offsetAmount >= Mathf.PI * 2)
            offsetAmount -= Mathf.PI * 2;

        float newY = Mathf.Lerp(-moveAmount, moveAmount, Mathf.Pow((Mathf.Sin(offsetAmount) + 1) * 0.5f, 2));
        gameObject.transform.position = startPosition + new Vector3(0, newY, 0);
        currentCooldown = Mathf.Max(currentCooldown - Time.deltaTime, 0);

        if (shootAt != null && bulletType != null)
        {
            if (currentCooldown <= 0)
            {
                Vector3 dirToTarget = (shootAt.transform.position - transform.position).normalized;
                Quaternion spawnRotation = Quaternion.FromToRotation(transform.right, dirToTarget);
                ZeppBullet bullet = Instantiate(bulletType, transform.position, spawnRotation) as ZeppBullet;

                bullet.Initialize(dirToTarget * 250, GetComponentInChildren<CapsuleCollider>());

                currentShootAmount++;
                if (currentShootAmount >= shootAmount)
                {
                    currentCooldown = 5.0f;
                    currentShootAmount = 0;
                }
                else
                {
                    currentCooldown = 0.5f;
                }
            }
        }
    }
}
