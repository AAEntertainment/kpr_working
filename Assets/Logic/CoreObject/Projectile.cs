﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody)), RequireComponent(typeof(Collider))]
public class Projectile : WorldObject
{
    public MeshRenderer body;

    public enum EActiveStatus
    {
        Active,
        Dying,
        Inactive
    }

    public bool Dying
    {
        get
        {
            return currentStatus == EActiveStatus.Dying;
        }
    }

    EActiveStatus currentStatus = EActiveStatus.Inactive;

    virtual public void Initialize()
    {

    }

    public void SetActiveStatus(Vector3 startVelocity, float moveDirection)
    {
        SetStatus(EActiveStatus.Active);
        OnActivated(startVelocity, moveDirection);
    }

    public void SetDyingStatus()
    {
        SetStatus(EActiveStatus.Dying);
    }

    public void SetInactiveStatus()
    {
        SetStatus(EActiveStatus.Inactive);
        OnDeactivated();
    }

    virtual public void OnActivated(Vector3 startVelocity, float moveDirection)
    {

    }

    virtual public void OnDeactivated()
    {

    }

    void SetStatus(EActiveStatus status)
    {
        currentStatus = status;
        switch (status)
        {
            case EActiveStatus.Active:
                enabled = true;
                break;
            case EActiveStatus.Inactive:
                enabled = false;
                break;
        }

        if(status == EActiveStatus.Active || status == EActiveStatus.Inactive)
        {
            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer renderer in renderers)
            {
                renderer.enabled = enabled;
            }

            GetComponent<Collider>().enabled = enabled;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }  
        else
        {
            if (body)
                body.enabled = false;
        }
    }
}
