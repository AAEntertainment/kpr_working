﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Math
{
    public enum EEaseType
    {
        /** Simple linear interpolation. */
        Linear,

        /** Sinusoidal in interpolation. */
        SinusoidalIn,

        /** Sinusoidal out interpolation. */
        SinusoidalOut,

        /** Sinusoidal in/out interpolation. */
        SinusoidalInOut,

        /** Smoothly accelerates, but does not decelerate into the target.  Ease amount controlled by BlendExp. */
        EaseIn,

        /** Immediately accelerates, but smoothly decelerates into the target.  Ease amount controlled by BlendExp. */
        EaseOut,

        /** Smoothly accelerates and decelerates.  Ease amount controlled by BlendExp. */
        EaseInOut,

        /** Easing in using an exponential */
        ExpoIn,

        /** Easing out using an exponential */
        ExpoOut,

        /** Easing in/out using an exponential method */
        ExpoInOut,

        /** Easing is based on a half circle. */
        CircularIn,

        /** Easing is based on an inverted half circle. */
        CircularOut,

        /** Easing is based on two half circles. */
        CircularInOut
    };

    public const float SMALL_NUMBER = (float)(1.0e-8f);
    public const float KINDA_SMALL_NUMBER = (float)(1.0e-4f);
    public const float BIG_NUMBER = (float)(3.4e+38f);
    public const float PI_HALF = Mathf.PI * 0.5f;
    public const float PI_X2 = Mathf.PI * 2.0f;

    // FLOAT

    public static float FEase(float A, float B, float alpha, EEaseType functionType, float blendExp = 2)
    {
        return Mathf.Lerp(A, B, Alpha(alpha, functionType, blendExp));
    }

    public static float FInterpTo(float current, float target, float interpSpeed)
    {
        if (interpSpeed <= 0.0f)
            return target;

        float dist = target - current;
        if (dist * dist < SMALL_NUMBER)
            return target;

        float deltaMove = dist * Mathf.Clamp(Time.deltaTime * interpSpeed, 0.0f, 1.0f);
        return current + deltaMove;
    }

    public static float FInterpConstantTo(float current, float target, float interpSpeed)
    {
        float dist = target - current;
        if (dist * dist < SMALL_NUMBER)
            return target;

        float step = interpSpeed * Time.deltaTime;
        return current + Mathf.Clamp(dist, -step, step);
    }

    public static float FAccelerateTo(float current, float target, float interpSpeed, float acceleration)
    {
        float dist = target - current;
        if (dist * dist < SMALL_NUMBER)
            return target;

        float step = (interpSpeed + (Mathf.Abs(current) * acceleration * Time.deltaTime)) * Time.deltaTime;
        return current + Mathf.Clamp(dist, -step, step);
    }

    // VECTOR

    public static Vector3 VEase(Vector3 A, Vector3 B, float alpha, EEaseType functionType, float blendExp = 2)
    {
        return Vector3.Lerp(A, B, Alpha(alpha, functionType, blendExp));
    }

    public static Vector3 VInterpConstantTo(Vector3 current, Vector3 target, float interpSpeed)
    {
        Vector3 delta = target - current;
        float deltaM = delta.magnitude;
        float maxStep = interpSpeed * Time.deltaTime;

        if (deltaM > maxStep)
        {
            if (maxStep > 0.0f)
            {
                Vector3 deltaN = delta / deltaM;
                return current + deltaN * maxStep;
            }
            else
            {
                return current;
            }
        }

        return target;
    }

    public static Vector3 VInterpTo(Vector3 current, Vector3 target, float interpSpeed)
    {
        if (interpSpeed <= 0.0f)
            return target;

        Vector3 dist = target - current;

        if (dist.sqrMagnitude < KINDA_SMALL_NUMBER)
            return target;

        Vector3 deltaMove = dist * Mathf.Clamp(Time.deltaTime * interpSpeed, 0.0f, 1.0f);

        return current + deltaMove;
    }


    // QUATERNION

    static Quaternion QEase(Quaternion A, Quaternion B, float alpha, bool shortestPath, EEaseType functionType, float blendExp = 2)
    {
        return Quaternion.Lerp(A, B, Alpha(alpha, functionType, blendExp));
    }

    public static float QAngularDistance(Quaternion A, Quaternion B)
    {
        float innerProd = A.x * B.x + A.y * B.y + A.z * B.z + A.w * B.w;
        return Mathf.Acos((2 * innerProd * innerProd) - 1.0f);
    }

    public static Quaternion QInterpConstantTo(Quaternion current, Quaternion target, float interpSpeed)
    {
        if (interpSpeed <= 0.0f)
            return target;

        if (current.Equals(target))
            return target;


        float deltaInterpSpeed = Mathf.Clamp(Time.deltaTime * interpSpeed, 0.0f, 1.0f);
        float angularDistance = Mathf.Max(SMALL_NUMBER, QAngularDistance(target, current));
        float alpha = Mathf.Clamp(deltaInterpSpeed / angularDistance, 0.0f, 1.0f);

        return QSlerp(current, target, alpha);
    }

    public static Quaternion QInterpTo(Quaternion current, Quaternion target, float interpSpeed)
    {
        if (interpSpeed <= 0.0f)
            return target;

        if (current.Equals(target))
            return target;

        return QSlerp(current, target, Mathf.Clamp(interpSpeed * Time.deltaTime, 0.0f, 1.0f));
    }

    public static Quaternion QLerp(Quaternion A, Quaternion B, float alpha, bool shortWay = true)
    {
        if (shortWay)
        {
            float dot = Quaternion.Dot(A, B);
            if (dot < 0.0f)
                Quaternion.Lerp(QScalarMultiply(A, -1.0f), B, alpha);
        }

        return Quaternion.Lerp(A, B, alpha);
    }

    public static Quaternion QSlerp(Quaternion A, Quaternion B, float alpha, bool shortWay = true)
    {
        float dot = Quaternion.Dot(A, B);
        if (shortWay)
        {
            if (dot < 0.0f)
                Quaternion.Slerp(QScalarMultiply(A, -1.0f), B, alpha);
        }

        return Quaternion.Slerp(A, B, alpha);
    }

    static Quaternion QScalarMultiply(Quaternion q, float scalar)
    {
        return new Quaternion(q.x * scalar, q.y * scalar, q.z * scalar, q.w * scalar);
    }


    // EASE

    public static float Alpha(float alpha, EEaseType functionType, float blendExp)
    {
        switch (functionType)
        {
            case EEaseType.Linear: return alpha;
            case EEaseType.SinusoidalIn: return FSinIn(0.0f, 1.0f, alpha);
            case EEaseType.SinusoidalOut: return FSinOut(0.0f, 1.0f, alpha);
            case EEaseType.SinusoidalInOut: return FSinInOut(0.0f, 1.0f, alpha);
            case EEaseType.EaseIn: return FEaseIn(0.0f, 1.0f, alpha, blendExp);
            case EEaseType.EaseOut: return FEaseOut(0.0f, 1.0f, alpha, blendExp);
            case EEaseType.EaseInOut: return FEaseInOut(0.0f, 1.0f, alpha, blendExp);
            case EEaseType.ExpoIn: return FExpoIn(0.0f, 1.0f, alpha);
            case EEaseType.ExpoOut: return FExpoOut(0.0f, 1.0f, alpha);
            case EEaseType.ExpoInOut: return FExpoInOut(0.0f, 1.0f, alpha);
            case EEaseType.CircularIn: return FCircularIn(0.0f, 1.0f, alpha);
            case EEaseType.CircularOut: return FCircularOut(0.0f, 1.0f, alpha);
            case EEaseType.CircularInOut: return FCircularInOut(0.0f, 1.0f, alpha);
        }

        return alpha;
    }

    public static float FCosLerp(float A, float AB, float B, float alpha)
    { 
        float modifiedAlpha = (Mathf.Cos(Mathf.Lerp(0, PI_X2, alpha)) + 1.0f) * 0.5f;
        if (alpha <= 0.5f)
            return Mathf.Lerp(A, AB, 1.0f - modifiedAlpha);
        return Mathf.Lerp(AB, B, modifiedAlpha);
    }

    public static float FSinIn(float A, float B, float alpha)
	{
        float modifiedAlpha = -1.0f * Mathf.Cos(alpha * Math.PI_HALF) + 1.0f;
		return Mathf.Lerp(A, B, modifiedAlpha);
	}

    public static float FSinOut(float A, float B, float alpha)
	{
        float modifiedAlpha = Mathf.Sin(alpha * Math.PI_HALF * 0.5f);
        return Mathf.Lerp(A, B, modifiedAlpha);
	}

    public static float FSinInOut(float A, float B, float alpha)
	{
		return Mathf.Lerp(A, B, (alpha < 0.5f) ?
            FSinIn(0.0f, 1.0f, alpha * 2.0f) * 0.5f :
            FSinOut(0.0f, 1.0f, alpha * 2.0f - 1.0f) * 0.5f + 0.5f);
	}

    public static float FEaseIn(float A, float B, float alpha, float exp)
	{
        float modifiedAlpha = Mathf.Pow(alpha, exp);
		return Mathf.Lerp(A, B, modifiedAlpha);
	}

    public static float FEaseOut(float A, float B, float alpha, float exp)
    {
        float modifiedAlpha = 1.0f - Mathf.Pow(1.0f - alpha, exp);
        return Mathf.Lerp(A, B, modifiedAlpha);
    }

    public static float FEaseInOut(float A, float B, float alpha, float exp)
    {
        return Mathf.Lerp(A, B, (alpha < 0.5f) ?
            FEaseIn(0.0f, 1.0f, alpha * 2.0f, exp) * 0.5f :
            FEaseOut(0.0f, 1.0f, alpha * 2.0f - 1.0f, exp) * 0.5f + 0.5f);
    }

    public static float FExpoIn(float A, float B, float alpha)
	{
        float modifiedAlpha = (alpha == 0.0f) ? 0.0f : Mathf.Pow(2.0f, 10.0f * (alpha - 1.0f));
		return Mathf.Lerp(A, B, modifiedAlpha);
	}

    public static float FExpoOut(float A, float B, float alpha)
    {
        float modifiedAlpha = (alpha == 1.0f) ? 1.0f : -Mathf.Pow(2.0f, -10.0f * alpha) + 1.0f;
        return Mathf.Lerp(A, B, modifiedAlpha);
    }

    public static float FExpoInOut(float A, float B, float alpha)
    {
        return Mathf.Lerp(A, B, (alpha < 0.5f) ?
            FExpoIn(0.0f, 1.0f, alpha * 2.0f) * 0.5f :
            FExpoOut(0.0f, 1.0f, alpha * 2.0f - 1.0f) * 0.5f + 0.5f);
    }

    public static float FCircularIn(float A, float B, float alpha)
	{
        float modifiedAlpha = -1.0f * (Mathf.Sqrt(1.0f - alpha * alpha) - 1.0f);
        return Mathf.Lerp(A, B, modifiedAlpha);
	}

    public static float FCircularOut(float A, float B, float alpha)
    {
        alpha -= 1.0f;
        float modifiedAlpha = Mathf.Sqrt(1.0f - alpha * alpha);
        return Mathf.Lerp(A, B, modifiedAlpha);
    }

    public static float FCircularInOut(float A, float B, float alpha)
    {
        return Mathf.Lerp(A, B, (alpha < 0.5f) ?
            FCircularIn(0.0f, 1.0f, alpha * 2.0f) * 0.5f :
            FCircularOut(0.0f, 1.0f, alpha * 2.0f - 1.0f) * 0.5f + 0.5f);
    }
}