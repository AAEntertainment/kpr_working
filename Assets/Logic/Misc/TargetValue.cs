﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
struct FTargetValue
{
    public FTargetValue(float initialValue, float lerpSpeed, float acceleration = 0)
    {
        current = initialValue;
        target = current;
        speed = lerpSpeed;
        acc = acceleration;
    }

    public float Update()
    {
        current = Math.FAccelerateTo(current, target, speed, acc);
        return current;
    }

    public float Value
    {
        set
        {
            target = value;
        }
        get
        {
            return current;
        }
    }

    public float Diff
    {
        get { return current - target; }
    }

    public void Reset(float toValue = 0.0f)
    {
        target = toValue;
        current = target;
    }

    public bool HasReachedTarget() { return current == target; }

    private float current;
    private float target;
    private float speed;
    private float acc;
};