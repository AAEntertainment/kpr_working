﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class TimeManager : MonoBehaviour
{
    private static readonly TimeManager instance = new TimeManager();
   
    // Explicit static constructor to tell C# compiler
    // not to mark type as beforefieldinit
    static TimeManager() { }

    float[] times;
    int timeIndex;
    readonly int maxIndex = 8;
    readonly int standardIndex = 5;

    private TimeManager()
    {
        times = new float[maxIndex];
        times[0] = 0.00001f;
        times[1] = 0.1f;
        times[2] = 0.25f;
        times[3] = 0.5f;
        times[4] = 0.75f;
        times[standardIndex] = 1.0f;
        times[6] = 1.5f;
        times[7] = 2.0f;

        timeIndex = 5;
    }

    public static TimeManager Instance
    {
        get
        {
            return instance;
        }
    }

    public void IncreaseTimeDilation()
    {
        SetTimeIndex(timeIndex + 1);
    }

    public void DecreaseTimeDilation()
    {
        SetTimeIndex(timeIndex - 1);
    }

    public void ResetTimeDilation()
    {
        SetTimeIndex(standardIndex);
    }

    private void SetTimeIndex(int newIndex)
    {
        timeIndex = Mathf.Clamp(newIndex, 0, maxIndex);
        Time.timeScale = times[timeIndex];
    }
}
