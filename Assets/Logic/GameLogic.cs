﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InputLogic))]
public class GameLogic : Singleton<GameLogic>
{
    [Header("Player")]
    public PlayerLogic playerType;
    PlayerLogic player;

    [Header("Level")]
    public LevelLogic level;

    [Header("GameLogic")]
    InputLogic input;

    float timeLeftToChangeGrafics = 0;

    public InputLogic InputLogic { get { return input; } }

    public PlayerLogic Player { get { return player; } }

    protected void OnValidate()
    {
        base.OnValidate();

        if (playerType == null)
            Debug.LogAssertion("GameLogic: playerType is needed!");

        if (level == null)
            Debug.LogAssertion("GameLogic: level is needed!");
    }

    void Start ()
    {
        input = GetComponent<InputLogic>();
        player = Instantiate(playerType, level.StartLocation, Quaternion.identity);
    }

    void Update ()
    {
        // Increase and decrease development game speed
#if !DEVELOPMENT_BUILD
        // Update the grafic settings if the fps is to low
        if (!gameObject.GetComponent<DebugLogic>().manualVariableGraficSettings)
#endif
        {
            int currentGraphSettings = QualitySettings.GetQualityLevel();
            float fps = 1.0f / (Time.deltaTime / Time.timeScale);
            if (fps < 20.0f)
            {
                timeLeftToChangeGrafics += Time.deltaTime;
                if (timeLeftToChangeGrafics > 5.0f && currentGraphSettings > 0)
                {
                    timeLeftToChangeGrafics = 0;
                    QualitySettings.DecreaseLevel();
                }
            }
            else if (fps > 30.0f && currentGraphSettings < 3)
            {
                timeLeftToChangeGrafics -= Time.deltaTime;
                if (timeLeftToChangeGrafics < 0.0f)
                {
                    timeLeftToChangeGrafics = 5.0f;
                    QualitySettings.IncreaseLevel();
                }
            }
        }      
    }
}
