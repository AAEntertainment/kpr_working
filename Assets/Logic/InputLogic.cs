﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class InputLogic : MonoBehaviour
{
    public Image inputStick;
    public ExtendUnityButtonLogic mainButton;
    public ExtendUnityButtonLogic secondaryButton;

    Rect inputRect = new Rect();
 
    void Start()
    {
#if !(MOBILE_INPUT)
        mainButton.gameObject.GetComponent<Image>().enabled = false;
        secondaryButton.gameObject.GetComponent<Image>().enabled = false;
        inputStick.gameObject.GetComponent<Image>().enabled = false;
#endif
        RectTransform[] allRectTransform = inputStick.GetComponentsInParent<RectTransform>();
        if(allRectTransform.Length > 0)
        {
            RectTransform rectTransform = allRectTransform[allRectTransform.Length - 1];
            Vector2 screenSize = new Vector2(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y);
            for (int i = allRectTransform.Length - 1; i >= 0; --i)
            {
                AdjustInputRectTransform(screenSize, ref allRectTransform[i]);
            }
        }
    }

    void AdjustInputRectTransform(Vector2 screenSize, ref RectTransform rectTransform)
    {
        Rect convertedRect = Rect.MinMaxRect(
            (rectTransform.anchorMin.x * screenSize.x) + rectTransform.offsetMin.x,
            (rectTransform.anchorMin.y * screenSize.y) + rectTransform.offsetMin.y,
            (rectTransform.anchorMax.x * screenSize.x) + rectTransform.offsetMax.x,
            (rectTransform.anchorMax.y * screenSize.y) + rectTransform.offsetMax.y);

        inputRect.Set(
            inputRect.position.x + convertedRect.position.x,
            inputRect.position.y + convertedRect.position.y,
            convertedRect.width,
            convertedRect.height);
    }

    void Update()
    {
        Vector2 inputAlpha = new Vector2();

#if (MOBILE_INPUT)
        Vector2 inputPosition = new Vector2();
        GetInputPosition(ref inputPosition, ref inputAlpha);
#else

        Vector3 mousePosition = Input.mousePosition;
        Vector3 screenPos = GameLogic.Instance.Player.Camera.GetWorldToScreenPosition(GameLogic.Instance.Player.Vehicle.transform.position);

        // Left - Right
        float alphaMove = Mathf.Clamp((mousePosition.x - screenPos.x) / 200.0f, -1.0f, 1.0f);
        if (Mathf.Abs(alphaMove) < 0.99f)
            inputAlpha.x = 0;
        else
            inputAlpha.x = Mathf.Sign(alphaMove);

        // Up - Down
        float alphaUpDownMove = Mathf.Clamp((mousePosition.y - screenPos.y) / 420.0f, -1.0f, 1.0f);
        inputAlpha.y = Mathf.Lerp(-1.0f, 1.0f, (alphaUpDownMove + 1) * 0.5f);
#endif

        GameLogic.Instance.Player.Vehicle.ApplyInputAlpha(inputAlpha);

        if (mainButton.InputType == ExtendUnityButtonLogic.EInputType.Held)
            GameLogic.Instance.Player.Vehicle.PressTopButton();
        else if (mainButton.InputType == ExtendUnityButtonLogic.EInputType.Released)
            GameLogic.Instance.Player.Vehicle.ReleaseTopButton();

        if (secondaryButton.InputType == ExtendUnityButtonLogic.EInputType.Held)
            GameLogic.Instance.Player.Vehicle.PressBottomButton();
        else if (secondaryButton.InputType == ExtendUnityButtonLogic.EInputType.Released)
            GameLogic.Instance.Player.Vehicle.ReleaseBottomButton();
    }

    Rect GetInputArea()
    {
        return inputRect;
    }

    bool GetAlpha(Vector2 inputPosition, ref Vector2 alpha)
    {
        Rect inputRect = GetInputArea();

        Vector2 normValue = Rect.PointToNormalized(inputRect, inputPosition);
        normValue.x = Mathf.Lerp(-1, 1, normValue.x);
        normValue.y = Mathf.Lerp(-1, 1, normValue.y);

        
        float dist = Vector2.Distance(inputPosition, inputRect.center);
        if (dist * Mathf.Abs(normValue.x) < inputRect.width * 0.5f &&
            dist * Mathf.Abs(normValue.y) < inputRect.height * 0.5f)
        {
            alpha.x = Vector2.Dot(normValue, new Vector2(1.0f, 0.0f));
            alpha.y = Vector2.Dot(normValue, new Vector2(0.0f, 1.0f));
            return true;
        }
        return false;
    }

    public int GetInputCount()
    {
#if (MOBILE_INPUT)
        return Input.touchCount;
#else
        return 0;
#endif
    }

    public bool GetInputPosition(ref Vector2 realValue, ref Vector2 alphaValue)
    {
#if (MOBILE_INPUT)
        for (int i = 0; i <  Input.touchCount; ++i)
        {
            Vector2 screenPosition = Input.GetTouch(i).position;
            if (GetAlpha(screenPosition, ref alphaValue))
            {
                realValue = screenPosition;
                return true;
            }
        }
#else
        realValue = Input.mousePosition;
        GetAlpha(Input.mousePosition, ref alphaValue);
#endif
        return false;
    }

}
