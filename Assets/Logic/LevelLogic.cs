﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLogic : MonoBehaviour
{
    public float minFlyHeight;
    public float maxFlyHeight;
    public Vector3 gravity = new Vector3(0, -200, 0);

    [Header("Start Location")]
    public GameObject optionalStartLocationObject;
    public Vector3 startLocation;

    void Start()
    {
        Physics.gravity = gravity;
    }

    public Vector3 StartLocation
    {
        get
        {
            if (optionalStartLocationObject != null)
                return optionalStartLocationObject.transform.position;
            else
                return startLocation;
        }
    }
}
