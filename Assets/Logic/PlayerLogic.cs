﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLogic : WorldObject
{
#if DEVELOPMENT_BUILD
    PlayerVehicleCore vehicle;
    PlayerCameraLogic playerCamera;
#endif

    void Start()
    {

#if DEVELOPMENT_BUILD
        vehicle = gameObject.GetComponentInChildren<PlayerVehicleCore>();
        playerCamera = gameObject.GetComponentInChildren<PlayerCameraLogic>();
#endif
        transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
        Vehicle.HealthSetup(OnDamageTaken, OnDamageDestroyed);
    }

    void OnValidate()
    {
        if (Vehicle == null)
            Debug.LogAssertion("PlayerLogic object needs a vehicle");

        if (Camera == null)
            Debug.LogAssertion("PlayerLogic object needs a camera");
    }

    override public void GetDebugInfo(ref string outString)
    {
        Vehicle.GetDebugInfo(ref outString);
    }

    void OnDamageTaken()
    {

    }

    void OnDamageDestroyed()
    {

    }

    void Update()
    {
        // Rotate the world
        Move(Vehicle.Velocity.z, Vehicle.MoveDirection);

        // Update camera 
        Camera.SetPosition(Vehicle.CameraHeightAlpha, Vehicle.MoveDirection);
    }

    public PlayerVehicleCore Vehicle
    {
        get
        {
#if DEVELOPMENT_BUILD
            return vehicle;
#else
            return gameObject.GetComponentInChildren<PlayerVehicleCore>();
#endif
        }
    }

    public PlayerCameraLogic Camera
    {
        get
        {
#if DEVELOPMENT_BUILD
            return playerCamera;
#else
            return gameObject.GetComponentInChildren<PlayerCameraLogic>();
#endif
        }
    }
}
