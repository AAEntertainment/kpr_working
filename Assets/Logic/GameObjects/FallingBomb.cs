﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DamageDealer))]
public class FallingBomb : Projectile
{
    public static readonly string Water = ("Water");
    public static readonly int WaterHash = Water.GetHashCode();

    enum EWaterStatus
    {
        None,
        WantToSink,
        Sinking
    }

    public GameObject rotation;

    [Header("Effect")]
    public EffectHandler explosionType;
    public EffectHandler shockWaveType;
    public EffectHandler waterImpactType;

    DamageDealer damage;
    Vector3 velocity;
    float direction;
    EWaterStatus sinkStatus;

    override public void Initialize()
    {
        // Bind the damage maker
        damage = GetComponent<DamageDealer>();
        damage.onImpact.AddListener(OnImpact); 
    }

    override public void OnActivated(Vector3 startVelocity, float moveDirection)
    {
        Vector3 currentRotation = rotation.transform.rotation.eulerAngles;
        currentRotation.x = 0;
        rotation.transform.rotation = Quaternion.Euler(currentRotation);
        direction = moveDirection;
        velocity = startVelocity;
        sinkStatus = EWaterStatus.None;
    }

    override public void OnDeactivated()
    {
        transform.position = Vector3.zero;
    }

    void Update ()
    {
        if (sinkStatus != EWaterStatus.Sinking && Dying == false)
        {
            // Rotate towards the velocity
            Vector3 currentRotation = rotation.transform.rotation.eulerAngles;
            currentRotation.x = Math.FInterpConstantTo(currentRotation.x, 90.0f, 100.0f);
            rotation.transform.rotation = Quaternion.Euler(currentRotation);

            // Increase the down velocity
            velocity.y -= Time.deltaTime * 30.0f;

            // Make the movement slow down
            velocity.z = Mathf.Max(velocity.z - (Time.deltaTime * 4.0f), 0);
        }

        // Move the bomb
        transform.Translate(Vector3.up * velocity.y * Time.deltaTime);

        // Rotate around the world
        transform.RotateAround(Vector3.zero, new Vector3(0, 1, 0), ((-Time.deltaTime) * velocity.z * direction));
    }

    void OnImpact(float damageDealth, GameObject other)
    {
        Tag tag = other.GetComponent<Tag>();
        if(tag && tag.Value == WaterHash)
        {
            //StartEffect(waterImpactType);
            float lifeTime = 0;
            if(waterImpactType != null)
                lifeTime = waterImpactType.CreateEffect();
            sinkStatus = EWaterStatus.WantToSink;
            Invoke("SetInactiveStatus", lifeTime);
        }
        else if (sinkStatus == EWaterStatus.WantToSink)
        {
            sinkStatus = EWaterStatus.Sinking;
            velocity.y *= 0.1f;
            velocity.z = 0.0f;
        }
        else
        {
            float lifeTime = 0;
            if(explosionType != null)
                lifeTime = explosionType.CreateEffect();

            if(shockWaveType != null)
                lifeTime = Mathf.Max(lifeTime, shockWaveType.CreateEffect());

            Invoke("SetInactiveStatus", lifeTime);

            SetDyingStatus();
            velocity = Vector3.zero;          
        }    
    }
}
