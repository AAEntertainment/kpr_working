﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DamageDealer))]
public class HorizontalMovingBullet : Projectile
{
    public float lifeTime = 1.5f;

    DamageDealer damage;
    Vector3 velocity;
    Vector3 initialRotateAround;
    float lifeTimeLeft = 0;

    public override void Initialize()
    {
        damage = GetComponent<DamageDealer>();
        damage.onImpact.AddListener(OnImpact);
    }

    override public void OnActivated(Vector3 startVelocity, float moveDirection)
    {
        // Rotate around the current up to get the real movement relative to the airplane
        initialRotateAround = transform.up;

        lifeTimeLeft = lifeTime;
        velocity = startVelocity;
        velocity.z *= moveDirection;
    }

    void Update()
    {
        transform.RotateAround(Vector3.zero, initialRotateAround, (-Time.deltaTime) * velocity.z);
        if (lifeTimeLeft > 0)
        {
            lifeTimeLeft -= Time.deltaTime;
            if (lifeTimeLeft <= 0)
            {
                SetInactiveStatus();
            }
        }
    }

    void OnImpact(float damageDealth, GameObject other)
    {
        SetInactiveStatus();
    }

    override public void OnDeactivated()
    {
        transform.position = new Vector3();
    }
}
