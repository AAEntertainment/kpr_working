﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeppBullet : MonoBehaviour
{
    Rigidbody physics;
    float lifeTimeLeft = 3;

    public void Initialize(Vector3 startVelocity, CapsuleCollider ignoreCollision)
    {
        physics = gameObject.GetComponent<Rigidbody>();
        physics.velocity = startVelocity;

        Collider collision = gameObject.GetComponent<Collider>();
        Physics.IgnoreCollision(ignoreCollision, collision);
    }

    void Update ()
    {
        lifeTimeLeft -= Time.deltaTime;
        if (lifeTimeLeft <= 0)
            Destroy(gameObject);

    }

    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
