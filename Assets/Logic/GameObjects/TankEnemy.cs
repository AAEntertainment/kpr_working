﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VehicleCore))]
public class TankEnemy : WorldObject
{
    public GameObject Canon;

    void Start()
    {
        GetComponent<VehicleCore>().HealthSetup(OnDamageTaken, OnDamageDestroyed);
    }

    // Update is called once per frame
    void Update()
    {
        //float distance = HorizontalAngularDistanceTo(player);
        //if (distance < 20 && distance > 5)
        //{
        //    float dir = player.FlyDirection;
        //    if (IsToTheRightOfPlayer() == false)
        //        dir *= -1;
        //    transform.RotateAround(Vector3.zero, new Vector3(0, 1, 0), (-Time.deltaTime * moveSpeed * dir));
        //}         
    }

    void OnDamageTaken()
    {

    }

    void OnDamageDestroyed()
    {
        Destroy(gameObject);
    }

}
