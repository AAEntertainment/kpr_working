﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shockwave : MonoBehaviour
{
    public float startScale = 0;
    public float endScale = 0;
    public float activeTime = 0;
    public Animator spriteAnimator;


    float currentTime = 0;
    float currentMultiplier = 1;

    public void Play()
    {
        if (activeTime <= 0)
            return;

        if (endScale <= 0)
            return;

        currentTime = 0;
        enabled = true;
        transform.localScale = Scale;
        Renderer[] renderers = GetComponents<Renderer>();
        foreach (Renderer rend in renderers)
        {
            rend.enabled = true;
        }

        float currrentTime = spriteAnimator.playbackTime;
        currentMultiplier = Mathf.Lerp(0.8f, 1.3f, Random.value);
        spriteAnimator.speed = currentMultiplier;
        spriteAnimator.playbackTime = 0.0f;
        spriteAnimator.enabled = true;
        // spriteAnimator.Play("Enabled", 1, 0);
    }

    public float GetAciveTime()
    {
        return activeTime / currentMultiplier;
    }

    public void Stop()
    {
        enabled = false;
        Renderer[] renderers = GetComponents<Renderer>();
        foreach (Renderer rend in renderers)
        {
            rend.enabled = false;
        }

        spriteAnimator.enabled = false;
        transform.localScale = Vector3.zero;
    }

    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime >= GetAciveTime())
        {
            Stop();
            return;
        }

        transform.localScale = Scale;
    }

    Vector3 Scale
    {
        get
        {
            float alpha = Mathf.Min(currentTime / activeTime, 1.0f);
            float scale = Mathf.Lerp(startScale, endScale, alpha);
            return new Vector3(scale, scale, scale) * 10;
        }
    }
}
