﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAirplane : PlayerVehicleCore
{
    [System.Serializable]
    public struct FBulletLocation
    {
        public Transform transform;
        public ParticleSystem muzzleFlash;

        private float muzzleFlashEffectStopTimeLeft;

        float EffectTimeLeft
        {
            get { return muzzleFlashEffectStopTimeLeft; }
            set { muzzleFlashEffectStopTimeLeft = value; }
        }

        public void Update()
        {
            if (muzzleFlashEffectStopTimeLeft > 0)
            {
                muzzleFlashEffectStopTimeLeft -= Time.deltaTime;
                if (muzzleFlashEffectStopTimeLeft <= 0)
                {
                    muzzleFlash.Stop();
                    muzzleFlashEffectStopTimeLeft = 0;
                }
            }
        }
    };

    public FallingBomb bombType;
    public float bombRate = 0.5f;
    public GameObject[] bombLocations;
    int bombLocationIndex = 0;
    float currentBombCooldown = 0;

    public HorizontalMovingBullet bulletType;
    public float fireRate = 0.1f;
    public FBulletLocation[] bulletLocations;
    int bulletLocationIndex = 0;
    float currentBulletCooldown = 0;

    [Header("Movement")]

    public float moveSpeedMin = 4;
    public float moveSpeedMax = 10;
    public float verticalIncreaseMoveSpeed = 40;
    public float verticalDecreaseMoveSpeed = 60;

    public float changeVelocitySpeed = 60.0f;
    public float changeVelocityAcceleration = 5.0f;
    public float changePitchSpeed = 3.0f;
    public float changeDirectionSpeed = 3.0f;
    public float changeDirectionDelay = 0.2f;

    [Range(0, 1)]
    public float startFlyHeightPercentage = 0.1f;
    public float minFlyHeight = 0;
    public float maxFlyHeight = 0;

    [Header("Airplane")]
    public GameObject airplaneBody;
    public GameObject airplaneEngine;

    float currentChangeRotationDelay;
    float lastMoveDirection = 0;

    FTargetValue moveDirection;
    FTargetValue pitchAmount;
    FTargetValue horizontalVelocity;
    FTargetValue verticalVelocity;

    ProjectileContainer bombContainer;
    ProjectileContainer bulletContainer;

    void Start()
    {
        currentChangeRotationDelay = changeDirectionDelay;

        bombContainer = gameObject.AddComponent(typeof(ProjectileContainer)) as ProjectileContainer;
        bombContainer.Initialize(bombType, 30, Collision);

        bulletContainer = gameObject.AddComponent(typeof(ProjectileContainer)) as ProjectileContainer;
        bulletContainer.Initialize(bulletType, 30, Collision);

        for (int i = 0; i < bulletLocations.Length; ++i)
        {
            bulletLocations[i].muzzleFlash.Stop();
        }
 
        // Start position
        transform.localPosition = new Vector3(0, Mathf.Lerp(minFlyHeight, maxFlyHeight, startFlyHeightPercentage), 0);
        moveDirection = new FTargetValue(1.0f, changeDirectionSpeed);
        pitchAmount = new FTargetValue(0.0f, changePitchSpeed);
        horizontalVelocity = new FTargetValue(moveSpeedMax, changeVelocitySpeed, changeVelocityAcceleration);
        verticalVelocity = new FTargetValue(0.0f, changeVelocitySpeed, changeVelocityAcceleration);
    }

    void Update()
    {
        // Update cooldowns
        currentBulletCooldown -= Time.deltaTime;
        currentBombCooldown -= Time.deltaTime;
        if (!IsTurning())
        {
            currentChangeRotationDelay = Mathf.Max(currentChangeRotationDelay - Time.deltaTime, 0);
        }

        for (int i = 0; i < bulletLocations.Length; ++i)
        {
            bulletLocations[i].Update();
        }

        // Update Engine
        airplaneEngine.transform.rotation = airplaneEngine.transform.rotation * Quaternion.Euler(Time.deltaTime * 1000, 0, 0);


        // Apply roll
        float rollAmount = 0.0f;

        // Add roll when pitching
        rollAmount += Mathf.Lerp(0, 25.0f * Mathf.Sign(pitchAmount.Value) * moveDirection.Value, Mathf.Abs(pitchAmount.Value));

        // Add roll when turning
        if (IsTurning())
        {
            rollAmount += Mathf.Lerp(0, 45.0f * (1.0f - Mathf.Abs(pitchAmount.Value)) * -lastMoveDirection, Mathf.Abs(moveDirection.Diff * 0.75f));
        }

        // Update internals
        moveDirection.Update();
        pitchAmount.Update();
        horizontalVelocity.Update();
        verticalVelocity.Update();

        // Update the height position
        IncreaseFlyHeight(Velocity.y * Time.deltaTime);

        // Update velocity
        UpdateVelocity();

        // Update plane rotation
        Quaternion targetRotation = Quaternion.Euler(
            rollAmount,
            transform.localRotation.eulerAngles.y,
            Pitch
            );

        transform.localRotation = Math.QInterpTo(
            transform.localRotation,
            targetRotation,
            10.0f);

        // Update plane direction rotation
        transform.localRotation = Quaternion.Euler(transform.localRotation.eulerAngles.x, Mathf.Lerp(179.0f, 1.0f, (moveDirection.Value + 1.0f) * 0.5f), transform.localRotation.eulerAngles.z);
    }

    override public void GetDebugInfo(ref string outString)
    {
        outString += "Direction: " + MoveDirection.ToString("f2") + " | Turning: " + IsTurning() + "\n";
        outString += "Velocity: " + Velocity.ToString("f2") + "\n";
    }

    override public void ApplyInputAlpha(Vector2 input)
    {
        // Update pitch
        float newValue = Math.FEaseIn(0.0f, 1.0f, Mathf.Abs(input.y), 2.0f) * Mathf.Sign(input.y);
        pitchAmount.Value = Mathf.Clamp(newValue * 1.25f, -1.0f, 1.0f);

        // Update direction
        if (IsTurning())
            return;

        if(Mathf.Abs(input.y) > 0.7f)
            return;

        if (Mathf.Abs(input.x) < 0.25f)
            return;

        if (currentChangeRotationDelay > 0)
            return;

        float wantedInput = Mathf.Sign(input.x);
        if (Mathf.Sign(moveDirection.Value) == wantedInput)
            return;

        lastMoveDirection = Mathf.Sign(moveDirection.Value);
        moveDirection.Value = wantedInput;
        currentChangeRotationDelay = changeDirectionDelay;
    }

    override public void PressTopButton()
    {
        if (currentBulletCooldown > 0)
            return;

        if (IsTurning())
            return;

        currentBulletCooldown = fireRate;
        bulletContainer.LaunchProjectile(BulletSpawnTransform, new Vector3(0, Velocity.y, Velocity.z + 20.0f), MoveDirection);

        // change the fire positions
        bulletLocationIndex++;
        if (bulletLocationIndex >= bulletLocations.Length)
            bulletLocationIndex = 0;

       

        //if (muzzleFlash != null)
        //{
        //    if (muzzleFlashEffectStopTimeLeft > 0)
        //        StopFlashEffect();

        //    muzzleFlashEffectStopTimeLeft = 0.17f;
        //    muzzleFlash.Play();
        //}
    }

    override public void ReleaseTopButton()
    {
        currentBulletCooldown = 0;
    }

    override public void PressBottomButton()
    {
        if (IsTurning())
            return;

        if (currentBombCooldown > 0)
            return;

        currentBombCooldown = bombRate;
        bombContainer.LaunchProjectile(BombSpawnTransform, new Vector3(0, Mathf.Min(Velocity.y, 0.0f), Velocity.z), MoveDirection);
    }

    override public void ReleaseBottomButton()
    {
        currentBombCooldown = 0;
    }

    public bool IsTurning()
    {
        return moveDirection.HasReachedTarget() == false;
    }

    override public Vector3 Velocity
    {
        get { return new Vector3(0.0f, verticalVelocity.Value, horizontalVelocity.Value); }
    }

    void UpdateVelocity()
    {
        float pitchSign = Mathf.Sign(pitchAmount.Value);
        float pitchSpeed = 0.0f;
        if (pitchSign > 0)
            pitchSpeed = Mathf.Lerp(0.0f, verticalIncreaseMoveSpeed, Mathf.Abs(pitchAmount.Value));
        else
            pitchSpeed = Mathf.Lerp(0.0f, verticalDecreaseMoveSpeed, Mathf.Abs(pitchAmount.Value));

        float pitchSpeedMultiplier = pitchAmount.Value > 0 ? pitchAmount.Value * 0.45f : Mathf.Abs(pitchAmount.Value * 1.45f);
        horizontalVelocity.Value = Mathf.Lerp(moveSpeedMin, moveSpeedMax, Mathf.Abs(moveDirection.Value)) + pitchSpeedMultiplier;
        verticalVelocity.Value = pitchSign * pitchSpeed;
    }

    public float Pitch
    {
        get { return Mathf.Lerp(-50.0f, 50.0f, (pitchAmount.Value + 1.0f) * 0.5f); }
        set{ pitchAmount.Value = value; }
    }

    public CapsuleCollider Collision
    {
        get
        {
            CapsuleCollider collider = airplaneBody.GetComponent<CapsuleCollider>();
            return collider;
        }
    }

    override public float MoveDirection { get { return moveDirection.Value; } }

    override public float CameraHeightAlpha { get { return FlyHeight / maxFlyHeight; } }

    public Transform BombSpawnTransform
    {
        get
        {
            Transform outTransform = bombLocations[bombLocationIndex].transform;
            outTransform.rotation = Quaternion.Euler(new Vector3(outTransform.rotation.eulerAngles.x, outTransform.rotation.eulerAngles.y, 0));
            return outTransform;
        }
    }

    public Transform BulletSpawnTransform
    {
        get
        {
            Transform outTransform = bulletLocations[bulletLocationIndex].transform;
           // outTransform.rotation = Quaternion.Euler(new Vector3(0, outTransform.rotation.eulerAngles.y, outTransform.rotation.eulerAngles.z));
            return outTransform;
        }
    }

    //public Vector3 BulletDirection { get { return airplaneBody.transform.forward; } }

    public float FlyHeight
    {
        set { transform.localPosition = new Vector3(0, value, 0); }
        get { return transform.localPosition.y; }
    }

    public void IncreaseFlyHeight(float amount)
    {
        float currentHeight = FlyHeight;
        currentHeight += Velocity.y * Time.deltaTime;

        // Clamp height
        if (currentHeight > maxFlyHeight)
        {
            pitchAmount.Reset();
            verticalVelocity.Reset();
            currentHeight = maxFlyHeight;
        }
        else if (currentHeight < minFlyHeight)
        {
            pitchAmount.Reset();
            verticalVelocity.Reset();
            currentHeight = minFlyHeight;
        }

        FlyHeight = currentHeight;
    }
}
