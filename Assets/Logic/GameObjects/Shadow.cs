﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : MonoBehaviour
{
    public GameObject origin;
    void Update()
    {
        if(origin != null)
        {
            Ray ray = new Ray(origin.transform.position, -Vector3.up);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, 200, gameObject.layer))
            {
                transform.position = hitInfo.point + (Vector3.up * 2);
                float alpha = Mathf.Lerp(0.4f, 0.05f, Mathf.Min(hitInfo.distance, 50) / 50);
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, alpha);
            }
        }
    }
}
