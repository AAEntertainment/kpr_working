﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugLogic : MonoBehaviour
{
    public bool drawPlayerStats = false;
    public bool drawInputStats = false;
    public bool drawCameraStats = false;
    public WorldObject drawDistanceToPlayerObject;
    public Color drawColor = Color.blue;

#if !(MOBILE_INPUT)
    public bool manualVariableGraficSettings = false;
#endif

#if !DEVELOPMENT_BUILD
    void Start()
    {

    }
#endif


#if !DEVELOPMENT_BUILD
    void Update()
    {
        // Time dilation
        if (Input.GetKeyDown("[+]"))
        {
            TimeManager.Instance.IncreaseTimeDilation();
        }
        else if (Input.GetKeyDown("[-]"))
        {
            TimeManager.Instance.DecreaseTimeDilation();
        }

        // Grafic settings
        if (manualVariableGraficSettings)
        {
            if (Input.GetKeyDown("[*]"))
            {
                QualitySettings.IncreaseLevel();
            }
            else if (Input.GetKeyDown("[/]"))
            {
                QualitySettings.DecreaseLevel();
            }
        }
    }
#endif

#if !DEVELOPMENT_BUILD
    void OnGUI()
    {
        float fps = 1.0f / (Time.smoothDeltaTime / Time.timeScale);
        Vector2 screenSize = new Vector2(Screen.width, Screen.height);

        string debugText = "";

        // Default debug info
        debugText +=
            "FPS = " + fps.ToString("f0") + " | Quality: " + QualitySettings.GetQualityLevel() + " | " + (" Time scale: ") + Time.timeScale + "\n" +
            "Screen: width = " + screenSize.ToString("f0") + "\n";

        // Input
        if (drawInputStats)
        {
            Vector2 inputAlpha = new Vector2();
            Vector2 inputPosition = new Vector2();
            GameLogic.Instance.InputLogic.GetInputPosition(ref inputPosition, ref inputAlpha);
            debugText += "Input = " + GameLogic.Instance.InputLogic.GetInputCount().ToString("f0") + " | Alpha: " + inputAlpha.ToString("f2") + " | Position: " + inputPosition.ToString("f2") + "\n";
        }

        // Player
        if (drawPlayerStats)
        {
            debugText += "PLAYER\n";
            GameLogic.Instance.Player.GetDebugInfo(ref debugText);
        }

        // Objects
        GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
        debugText += "OBJECTS (" + allObjects.Length.ToString() + ")\n";
        if (drawDistanceToPlayerObject != null)
        {
            debugText += "Distance to: " + drawDistanceToPlayerObject.name + ": " + GameLogic.Instance.Player.HorizontalAngularDistanceTo(drawDistanceToPlayerObject).ToString("f2") + "\n";
        }

        // Camera
        if (drawCameraStats)
        {
            debugText += "CAMERA\n";
            GameLogic.Instance.Player.Camera.GatherDebugInfo(ref debugText);
        }

        GUI.skin.label.fontSize = 24;
        GUI.color = drawColor;
        GUI.Label(new Rect(screenSize.x * 0.1f, 20, screenSize.x * 0.75f, screenSize.y - 20), debugText);
    }
#endif
        }
