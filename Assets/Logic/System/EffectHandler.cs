﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EffectHandler : MonoBehaviour
{
    enum EffectType { None, Sprite, ParticleLow, ParticleHigh, };

    // Sprite effect
    public SpriteEffect2D spriteEffectType;

    // Particle effect
    public ParticleSystem particleEffectLowType;
    public ParticleSystem particleEffectType;

    EffectType activeEffectType = EffectType.None;
    float lifeTime = 0;

    private void Start()
    {
        if(spriteEffectType != null)
            spriteEffectType.gameObject.SetActive(false);

        if (particleEffectLowType != null)
            particleEffectLowType.gameObject.SetActive(false);

        if (particleEffectType != null)
            particleEffectType.gameObject.SetActive(false);
    }

    public float CreateEffect()
    {
        lifeTime = 0;
        activeEffectType = EffectType.None;

        switch (WantedEffectQuality)
        {
            case EffectType.Sprite:

                if (spriteEffectType != null)
                {
                    activeEffectType = EffectType.Sprite;
                    spriteEffectType.gameObject.SetActive(true);
                    lifeTime = spriteEffectType.LifeTime;
                    spriteEffectType.Reset();
                }
                break;

            case EffectType.ParticleLow:
                if (particleEffectLowType != null)
                {
                    activeEffectType = EffectType.ParticleLow;
                    particleEffectLowType.gameObject.SetActive(true);
                    //particleInstance = Instantiate(particleEffectLowType, creationPosition, Quaternion.identity);
                    // particleInstance = particleEffectLowType;
                    lifeTime = particleEffectLowType.main.duration;
                }
                break;

            case EffectType.ParticleHigh:
                if (particleEffectType != null)
                {
                    activeEffectType = EffectType.ParticleHigh;
                    particleEffectType.gameObject.SetActive(true);
                    //particleInstance = Instantiate(particleEffectType, creationPosition, Quaternion.identity);
                    lifeTime = particleEffectType.main.duration;
                }
                break;
        }

        if (activeEffectType == EffectType.None)
        {
            Debug.LogWarning("ScaleableEffect need at least on of a sprite or particle effect", this);
        }

        Invoke("DestroyEffect", lifeTime);

        return lifeTime;
    }

    public void DestroyEffect()
    {
        if (activeEffectType == EffectType.Sprite)
        {
            spriteEffectType.gameObject.SetActive(false);
        }
        else if (activeEffectType == EffectType.ParticleLow)
        {
            particleEffectLowType.gameObject.SetActive(false);
        }
        else if (activeEffectType == EffectType.ParticleHigh)
        {
            particleEffectType.gameObject.SetActive(false);
            //Destroy(spriteInstance.gameObject);
            //spriteInstance = null;
        }

        activeEffectType = EffectType.None;
    }

    float LifeTime
    {
        get
        {
            return lifeTime;
        }
    }

    EffectType WantedEffectQuality
    {
        get
        {
            //return EffectType.Sprite;

            int currentGraphSettings = QualitySettings.GetQualityLevel();
            switch (currentGraphSettings)
            {
#if (MOBILE_INPUT)
                case 0:
                case 1:
                    return EffectType.Sprite;
                defaut:
                    return EffectType.LowPartice;
                
#else
                case 0:
                case 1:
                    return EffectType.Sprite;
                case 2:
                case 3:
                case 4:
                    return EffectType.ParticleLow;
                default:
                    return EffectType.ParticleHigh;
#endif
            }       
        }   
    }
}
