﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageReceiver : MonoBehaviour
{
    public float maxHealth = 1.0f;
    public bool armored = false;
    public UnityEvent onDamageTaken;
    public UnityEvent onDamageDestroyed;

    private float currenthealth;

    private void Start()
    {
        currenthealth = maxHealth;
    }

    public float ApplyDamage(DamageDealer damageMaker)
    {
        if (armored && !damageMaker.armorPiercing)
            return 0;

        onDamageTaken.Invoke();

        float tempHealth = currenthealth;
        currenthealth -= damageMaker.damageAmount;
        if (currenthealth <= 0)
        {
            onDamageDestroyed.Invoke();
            currenthealth = 0;
        }

        return tempHealth - currenthealth;
    }
}
