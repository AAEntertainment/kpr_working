﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator)), RequireComponent(typeof(SpriteRenderer))]
public class SpriteEffect : MonoBehaviour
{
    // The end scale is the scale mulitplied with this
    public float endScaleMultiplier = 1;

    /* How long until max scale is reached
     * < 0; lifetime is used
    */
    public float scaleTime = -1;


    Animator spriteAnimator = null;
    Vector3 startScale;
    float currentTime;

    void Start()
    {
        spriteAnimator = GetComponentInParent<Animator>();
        startScale = transform.localScale;
        currentTime = 0;
    }

    void Update()
    {
        if (LifeTime > 0 && currentTime < LifeTime)
        {
            currentTime += Time.deltaTime;
            float alpha = Mathf.Min(currentTime / LifeTime, 1.0f);
            transform.localScale = Vector3.Lerp(startScale, startScale * endScaleMultiplier, alpha);
        }
    }

    void LateUpdate()
    {
        Quaternion cameraRotation = GameLogic.Instance.Player.Camera.transform.rotation;
        transform.LookAt(transform.position + (cameraRotation * Vector3.back), cameraRotation * Vector3.up);
    }

    public float LifeTime
    {
        get
        {
            if (spriteAnimator != null)
                return spriteAnimator.GetCurrentAnimatorStateInfo(0).length;
            else
                return 0;
        }
    }
}
