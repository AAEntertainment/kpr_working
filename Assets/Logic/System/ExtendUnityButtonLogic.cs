﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ExtendUnityButtonLogic : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public enum EInputType { None, Pressed, Held, Released, };
    public enum EMouseInputType { None, Left, Right, };

    public EMouseInputType mouseInput = EMouseInputType.None;

    bool isPressed = false;
    bool wasPressed = false;
    EInputType currentInputType = EInputType.None;
    
    public void OnPointerDown(PointerEventData eventData)
    {
#if (MOBILE_INPUT)
        isPressed = true;
#endif
    }

    public void OnPointerUp(PointerEventData eventData)
    {
#if (MOBILE_INPUT)
        isPressed = false;
#endif
    }

    void Update()
    {

#if !(MOBILE_INPUT)
        if (mouseInput == EMouseInputType.Left)
            isPressed = Input.GetMouseButton(0);
        else if (mouseInput == EMouseInputType.Right)
            isPressed = Input.GetMouseButton(1);
#endif

        if (isPressed)
        {
            if (wasPressed == isPressed)
                currentInputType = EInputType.Held;
            else if(!wasPressed)
                currentInputType = EInputType.Pressed;
        }
        else
        {
            if (wasPressed == isPressed)
                currentInputType = EInputType.None;
            else if (wasPressed)
                currentInputType = EInputType.Released;
        }

        wasPressed = isPressed;
    }

    public EInputType InputType { get { return currentInputType; } }
}
