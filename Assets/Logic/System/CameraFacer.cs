﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFacer : MonoBehaviour
{
    void LateUpdate()
    {
        Quaternion cameraRotation = GameLogic.Instance.Player.Camera.transform.rotation;
        transform.LookAt(transform.position + (cameraRotation * Vector3.back), cameraRotation * Vector3.up);

    }
}
