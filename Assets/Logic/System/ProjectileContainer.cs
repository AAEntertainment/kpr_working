﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileContainer : MonoBehaviour
{
    ArrayList projectiles;
    int activeIndex = 0;

    // Start is called before the first frame update
    public void Initialize(Projectile projectileType, int maxProjectiles, Collider parentCollider)
    {
        projectiles = new ArrayList(maxProjectiles);
        for (int i = 0; i < maxProjectiles; ++i)
        {
            Projectile projectile = Instantiate(projectileType, Vector3.zero, Quaternion.identity) as Projectile;
            projectile.Initialize();
            projectile.SetInactiveStatus();

            Collider collision = projectile.GetComponent<Collider>();
            if(parentCollider)
            {
                Physics.IgnoreCollision(collision, parentCollider);
            }

            for (int j = 0; j < projectiles.Count; ++j)
            {
                Projectile otherProjectile = projectiles[j] as Projectile;
                Collider otherCollision = otherProjectile.GetComponent<Collider>();
                Physics.IgnoreCollision(collision, otherCollision);
            }
            
            projectiles.Add(projectile);
        }
    }

    public void LaunchProjectile(Transform spawnTransform, Vector3 startVelocity, float moveDirection)
    {
        Projectile projectile = projectiles[activeIndex] as Projectile;

        activeIndex++;
        if (activeIndex == projectiles.Count)
            activeIndex = 0;

        projectile.transform.position = spawnTransform.position;
        projectile.transform.rotation = spawnTransform.rotation;
        projectile.SetActiveStatus(startVelocity, moveDirection);
    }
}
