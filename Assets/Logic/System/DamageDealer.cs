﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageDealer : MonoBehaviour
{
    public float damageAmount = 1.0f;
    public bool armorPiercing = false;
    public OnImpactEvent onImpact;

    private void OnTriggerEnter(Collider other)
    {
        DamageReceiver damageRecevier = other.gameObject.GetComponentInParent<DamageReceiver>();
        float damageDealth = 0;
        if (damageRecevier != null)
        {
            damageDealth = damageRecevier.ApplyDamage(this);
        }

        onImpact.Invoke(damageDealth, other.gameObject);
    }

    [System.Serializable]
    public class OnImpactEvent : UnityEvent<float, GameObject>
    {
    }
}
