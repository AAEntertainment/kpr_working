﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tag : MonoBehaviour
{
    public string nameTag = "";
    int hashedNameTag;

    void Start()
    {
        hashedNameTag = nameTag.GetHashCode();
        enabled = false;
    }

    public int Value { get { return hashedNameTag; } }
}
