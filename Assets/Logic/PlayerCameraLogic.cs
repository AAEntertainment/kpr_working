﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class PlayerCameraLogic : MonoBehaviour
{
    public float alphaModifyExponetial = 2.0f;
    public Math.EEaseType alphaModifyFunctionType = Math.EEaseType.Linear;

    public float cameraModifyExponetial = 2.0f;
    public Math.EEaseType cameraModifyFunctionType = Math.EEaseType.Linear;

    public float offsetAtMinHeight = 0.0f;
    public float offsetAtMaxHeight = 0.0f;

    Camera controlledCamera;
    Vector3 defaultPosition;

    bool gathterDebug = false;
    string debugString = "";

    void Start()
    {
        controlledCamera = GetComponent<Camera>();
        defaultPosition = controlledCamera.transform.localPosition;
        controlledCamera.transform.localPosition = new Vector3(defaultPosition.x, 0, defaultPosition.z);
    }

    public void SetPosition(float heightAlpha, float moveDirectionAlpha)
    {
        float modAlpha = Math.Alpha(heightAlpha, alphaModifyFunctionType, alphaModifyExponetial);
        float verticalOffset = Math.FEase(offsetAtMinHeight, offsetAtMaxHeight, modAlpha, cameraModifyFunctionType, cameraModifyExponetial);
        float horizontalOffset = moveDirectionAlpha * defaultPosition.x;

        controlledCamera.transform.localPosition = new Vector3(horizontalOffset, verticalOffset, defaultPosition.z);


#if !DEVELOPMENT_BUILD
        if (gathterDebug)
        {
            gathterDebug = false;
            debugString = "";
            debugString += "heightAlpha: " + heightAlpha + "\n";
            debugString += "cameraAlpha: " + modAlpha + "\n";
            debugString += "verticalPosition: " + verticalOffset + "\n";
        }
#endif

    }

    public void GatherDebugInfo(ref string outString)
    {
        gathterDebug = true;
        outString += debugString;
    }

    public Vector3 GetWorldToScreenPosition(Vector3 worldPosition)
    {
        return controlledCamera.WorldToScreenPoint(worldPosition);
    }
}
